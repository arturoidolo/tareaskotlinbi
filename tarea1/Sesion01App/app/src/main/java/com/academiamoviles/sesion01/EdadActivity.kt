package com.academiamoviles.sesion01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_edad.*
import kotlinx.android.synthetic.main.activity_edad.tvEdadMessage


class EdadActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edad)
        //FindViewById
        val btnConfirm : Button = findViewById(R.id.btnEdadConfirm)
        val edtNames : EditText = findViewById(R.id.edtEdad)

        btnEdadConfirm.setOnClickListener {

            //2. Get the values
            var edad = edtEdad.text.toString()
            var edadInt=edad.toInt()
            var resultado="Mayor"
            if (edadInt<18){
                resultado="Menor"
            }
            //3. Concat
            val rango = "Usted es $resultado de Edad "

            //4. Set in TextView
            tvEdadMessage.text = rango
        }
    }
}