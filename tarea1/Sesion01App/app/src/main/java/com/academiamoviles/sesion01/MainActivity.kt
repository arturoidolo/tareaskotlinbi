package com.academiamoviles.sesion01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //FindViewById
        val btnConfirm : Button = findViewById(R.id.btnConfirm)
        val edtNames : EditText = findViewById(R.id.edtNames)

        //Kotlyn Syntetic / Kotlin Extension / Deprecated

        //1. OnClick Event
        btnConfirm.setOnClickListener {

            //2. Get the values
            val names = edtNames.text.toString()
            val sureName = edtSureName.text.toString()

            //3. Concat
            val fullName = "Bienvenido a la aplicacion $names $sureName"

            //4. Set in TextView
            tvMessage.text = fullName
        }
    }

    /*
       //Variables
       //Variables mutables
       var names = "Juan Jose"
       var age  = 32
       var price = 20.0
       var condition  = true
       var character  = 'J'

       names = "Juan Jose Ledesma"

       //Variables immutables
       val pi = 3.1416
        */


}