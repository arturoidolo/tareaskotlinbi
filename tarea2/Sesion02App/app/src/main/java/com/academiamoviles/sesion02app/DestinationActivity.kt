package com.academiamoviles.sesion02app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class DestinationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination)

        //1. Received Bundle
        val bundle = intent.extras

        //SCOPE FUNTIONS LET

        //2. Get values
        //bundle?.let{ bundleNotNull ->
        val names = bundle?.getString("KEY_NAMES") ?: "Desconocido"
        val age = bundle?.getString("KEY_AGE") ?: "Desconocido"
        val gender = bundle?.getString("KEY_GENDER") ?: "Desconocido"

        //3. Set
        println("$names $age $gender")
        //}

        //if(bundle != null) {
        }

}