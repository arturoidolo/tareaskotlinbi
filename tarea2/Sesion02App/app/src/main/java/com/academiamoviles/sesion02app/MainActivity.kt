package com.academiamoviles.sesion02app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val MALE = "MALE"
    val FEMALE = "FEMALE"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //FindViewById
        /*val edtNames : EditText = findViewById(R.id.edtNames)
        val edtAge : EditText = findViewById(R.id.edtAge)
        val rbMale : RadioButton = findViewById(R.id.rbMale)
        val rbFemale : RadioButton = findViewById(R.id.rbFemale)
        val chkTermsConditions : CheckBox = findViewById(R.id.chkTermsConditions)
        val btnSend : Button = findViewById(R.id.btnSend)*/

        //1. Onclick event
        btnSend click {
            sendData()
        }

        //btnSend ::click
    }

    fun sendData(){
        //2. Get data
        val names = edtNames.text.toString()
        val age = edtAge.text.toString()
        val gender = if(rbMale.isChecked) MALE else FEMALE

        //3. Validations
        if(names.isEmpty()){
            showMessage(getString(R.string.validation_names))
            return
        }

        if(age.isEmpty()){
            showMessage(getString(R.string.validation_age))
            Toast.makeText(this,"",Toast.LENGTH_LONG).show()
            return
        }

        //if(!rbMale.isChecked && !rbMale.isChecked){ }

        if(!chkTermsConditions.isChecked){
            showMessage(getString(R.string.validation_terms_conditions))
            return
        }

        //APPLY - SCOPE FUNCTION
        //4. Store data / Bundle
        val bundle = Bundle().apply {
            putString("KEY_NAMES",names)
            putString("KEY_AGE",age)
            putString("KEY_GENDER",gender)
        }

        //5. Navigation
        val intent = Intent(this,DestinationActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)

        /*openActivity(DestinationActivity::class.java){
            it.putString("KEY_NAMES",names)
            it.putString("KEY_AGE",age)
            it.putString("KEY_GENDER",gender)
        }*/


        operatorFun(2,3) { x, y ->
            x+y
        }

        hello("Juan Jose"){ name ->
            println(name)
        }


        //Infix Functions call
        //val result = 2 plus 4

    }

    //Lambdas
    fun operatorFun(x:Int,y:Int,myFun:(Int,Int)->Int):Int{
        return myFun(x,y)
    }

    fun hello(name:String,hi:(String) -> Unit){
        hi(name)
    }

    /*fun add(number1:Int,number2:Int) : Int {
        return number1+number2
    }
    fun add2(number1:Int,number2:Int) = number1+number2*/







}