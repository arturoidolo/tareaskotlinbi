package com.academiamoviles.sesion02app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_mascota.*

class MascotaActivity : AppCompatActivity() {
    val DOG = "PERRO"
    val CAT = "GATO"
    val RABBIT = "CONEJO"
    val VACUM1 = "VACUNA 1"
    val VACUM2 = "VACUNA 2"
    val VACUM3 = "VACUNA 3"
    val VACUM4 = "VACUNA 4"
    val VACUM5 = "VACUNA 5"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mascota)
        //1. Onclick event
        btnSendMascotas click {
            sendData()
        }

        //btnSend ::click
    }

    fun sendData() {
        //2. Get data
        val names = edtNamesMascotas.text.toString()
        val age = edtAgeMascotas.text.toString()
        val gender = if (rbDog.isChecked) DOG else if(rbCat.isChecked) CAT else RABBIT
        val vacum1 = if (chkVacuna1.isChecked) VACUM1 else null
        val vacum2 = if (chkVacuna2.isChecked) VACUM2 else null
        val vacum3 = if (chkVacuna3.isChecked) VACUM3 else null
        val vacum4 = if (chkVacuna4.isChecked) VACUM4 else null
        val vacum5 = if (chkVacuna5.isChecked) VACUM5 else null
        //3. Validations
        if (names.isEmpty()) {
            showMessage(getString(R.string.validation_names))
            return
        }

        if (age.isEmpty()) {
            showMessage(getString(R.string.validation_age))
            Toast.makeText(this, "", Toast.LENGTH_LONG).show()
            return
        }

        if(!rbDog.isChecked && !rbDog.isChecked){ }

       // if (!chkTermsConditions.isChecked) {
       //     showMessage(getString(R.string.validation_terms_conditions))
       //     return
       // }

        //APPLY - SCOPE FUNCTION
        //4. Store data / Bundle
        val bundle = Bundle().apply {
            putString("KEY_NAMES", names)
            putString("KEY_AGE", age)
            putString("KEY_GENDER", gender)
            putString("KEY_VACUM1", vacum1)
            putString("KEY_VACUM2", vacum2)
            putString("KEY_VACUM3", vacum3)
            putString("KEY_VACUM4", vacum4)
            putString("KEY_VACUM5", vacum5)
        }

        //5. Navigation
        val intent = Intent(this, MascotaDestinationActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)
    }
}