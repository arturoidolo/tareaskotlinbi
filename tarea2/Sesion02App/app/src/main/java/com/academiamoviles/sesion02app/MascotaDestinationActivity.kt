package com.academiamoviles.sesion02app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_mascota_destination.*
import kotlinx.android.synthetic.main.activity_mascota_destination.view.*

class MascotaDestinationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mascota_destination)
        //1. Received Bundle
        val bundle = intent.extras

        //SCOPE FUNTIONS LET

        //2. Get values
        //bundle?.let{ bundleNotNull ->
        val names = bundle?.getString("KEY_NAMES") ?: "Desconocido"
        val age = bundle?.getString("KEY_AGE") ?: "Desconocido"
        val gender = bundle?.getString("KEY_GENDER") ?: "Desconocido"
        val vacum1 = bundle?.getString("KEY_VACUM1") ?: ""
        val vacum2 = bundle?.getString("KEY_VACUM2") ?: ""
        val vacum3 = bundle?.getString("KEY_VACUM3") ?: ""
        val vacum4 = bundle?.getString("KEY_VACUM4") ?: ""
        val vacum5 = bundle?.getString("KEY_VACUM5") ?: ""

        //3. Set
        textViewMascotasNameDes.text=names
        textViewMascotasAgeDes.text=age
        textViewMascotasRazDes.text=gender

        if (gender=="GATO"){
            imageView.setImageResource(R.drawable.cat)
        }
        if (gender=="PERRO"){
            imageView.setImageResource(R.drawable.dog)
        }
        if (gender=="CONEJO"){
            imageView.setImageResource(R.drawable.rabbit)
        }


        textViewMascotasVacunasDes.text=vacum1+" "+vacum2+" "+vacum3+" "+vacum4+" "+vacum5

        println("$names $age $gender")
        //}

        //if(bundle != null) {
    }

}