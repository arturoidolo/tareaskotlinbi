package com.academiamoviles.sesion02app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast

//Functions Extension
fun Context.showMessage(message:String) = Toast.makeText(this,message, Toast.LENGTH_LONG).show()

//Funciones Infix
infix fun Int.plus(number:Int) = this + number

infix fun View.click(click:()->Unit){
    setOnClickListener { click()  }
}

/*fun <T> Context.openActivity(it: Class<T>, extras:(Bundle) -> Unit) {
    val intent = Intent(this, it)
    intent.putExtras(Bundle().apply(extras))
    startActivity(intent)
}*/