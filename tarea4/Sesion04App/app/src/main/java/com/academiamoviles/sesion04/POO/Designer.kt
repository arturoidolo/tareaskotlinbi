package com.academiamoviles.sesion04.POO

class Designer(names:String, surName:String,val toolsNumbers:Int) : Worker(names,surName) {

    override fun work() {
        println("El diseñador esta trabajando")
    }
}