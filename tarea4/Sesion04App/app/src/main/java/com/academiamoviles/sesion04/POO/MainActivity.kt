package com.academiamoviles.sesion04.POO

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.academiamoviles.sesion04.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), Operacion {

    private var binding : ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        //setContentView(R.layout.activity_main)

        //Lists
        //Immutable
        var daysOfWeek  = listOf("Lunes","Martes","Miercoles", "Jueves","Viernes","Sabado","Domingo")

        //Mutables
        val personNames : MutableList<String> = mutableListOf()
        personNames.add("Juan Jose")
        personNames.add("Diego")

        println(daysOfWeek.size)  //7
        println(daysOfWeek[0]) //Lunes
        println(daysOfWeek.first()) //Lunes
        println(daysOfWeek.last()) //Domingo

        for(day in daysOfWeek){
           println("$day")
        }

        daysOfWeek.forEach { day ->
            println("$day")
        }



        println(
            daysOfWeek.filter { day ->
            day == "Viernes" || day == "Sabado" || day == "Domingo" }
        )

        val notes = listOf(18,20,12,14,17,10)

        val aprroved =  notes.filter {
            it >= 13
        }
        println(aprroved)

        val juanjose = Person("Juan Jose","Ledesma")
        println(juanjose.names)
        println(juanjose.surName)

        println(Person.greeting)

        val diego = Person().apply {
            names = "Diego"
            surName = "Moscoso"
        }

        val juanjosecopy = juanjose.copy(surName = "Ledesma Zevallos")

        val (names,surname) = Person("Rodrigo","Ledesma")
        println("$names $surname")

        if(juanjose == juanjosecopy){
            println("Son iguales")
        }else{
            println("No Son iguales")
        }

        println(juanjose.toString())

        val arturo = Developer("Arturo","Ayala",4)
        arturo.names
        arturo.work()

        val jaime = Designer("Jaime","Ospina",2)
        jaime.surname
        jaime.work()



    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onRestart() {
        super.onRestart()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun add(number1: Int, number2: Int): Int {
        return number1 + number2
    }

    override fun subtract(number1: Int, number2: Int): Int {
        return number1 - number2
    }

}