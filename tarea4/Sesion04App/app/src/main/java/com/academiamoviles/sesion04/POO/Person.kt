package com.academiamoviles.sesion04.POO

data class Person (var names:String, var surName:String){

    //static
    companion object{
        var greeting:String = "HOLA"

        fun createDatabase(){

        }
    }

    //Sobrecarga de constructores
    constructor(names:String):this(names,"")
    constructor():this("","")


}