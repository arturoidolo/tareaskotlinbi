package com.academiamoviles.sesion04.lists

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.academiamoviles.sesion04.databinding.ActivityContactoBinding


class ContactoActivity : AppCompatActivity() {
    private lateinit var binding : ActivityContactoBinding
    private lateinit var adapter : ContactosAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContactoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(R.layout.activity_contacto)

        configureAdapter()

        loadData()

        events()
    }

    private fun configureAdapter(){
        adapter = ContactosAdapter()
        binding.recyclerContactos.adapter = adapter

    }
    private fun loadData() {


    }

    private fun events() {

        val contactos = listOf(
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com"),
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com"),
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com"),
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com"),
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com"),
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com"),
            Contactos("Arturo Ayala","Desarrollador", "arturoidolo@gmail.com")
             )
        adapter.updateList(contactos)
    }

}