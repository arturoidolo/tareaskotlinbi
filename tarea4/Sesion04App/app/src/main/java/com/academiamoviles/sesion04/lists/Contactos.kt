package com.academiamoviles.sesion04.lists

import java.io.Serializable

data class Contactos(
    val nombre:String,
    val cargo:String,
    val correo:String) : Serializable

