package com.academiamoviles.sesion04.lists

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiamoviles.sesion04.R
import com.academiamoviles.sesion04.databinding.ItemContactosBinding
import com.squareup.picasso.Picasso


class ContactosAdapter(
    var contactos:List<Contactos> = listOf())  // (Contactos) -> Unit

    : RecyclerView.Adapter<ContactosAdapter.ViewHolder>(){

        inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView){

            private val binding : ItemContactosBinding = ItemContactosBinding.bind(itemView)

            //Scope Functions - Let / Apply / with
            fun bind(contactos:Contactos ) = with(binding) {
                ContactoNombre.text = contactos.nombre
                ContactoCargo.text=contactos.cargo
                ContactoCorreo.text=contactos.correo
                ContactoLetra.text=contactos.nombre.substring(1,1)


            }

        }

        fun updateList(contactos:List<Contactos>){
            this.contactos = contactos
            notifyDataSetChanged()
        }

        //Inflar una vista  //-Vista
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_contactos,parent,false)
            return ViewHolder(view)

        }

        //Ejecutar tantas veces como elementos tenga mi lista
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val contactos = contactos[position]
            holder.bind(contactos)
        }

        //Cuantos elementos tiene la lista
        override fun getItemCount(): Int {
            return contactos.size
        }
}