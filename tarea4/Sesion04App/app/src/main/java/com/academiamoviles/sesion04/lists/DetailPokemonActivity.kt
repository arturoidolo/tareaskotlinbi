package com.academiamoviles.sesion04.lists

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.academiamoviles.sesion04.databinding.ActivityDetailPokemonBinding
import com.squareup.picasso.Picasso

class DetailPokemonActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDetailPokemonBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPokemonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(R.layout.activity_detail_pokemon)

        val bundle = intent.extras

        bundle?.let{

            val pokemon = it.getSerializable("KEY_POKEMON") as Pokemon

            binding.tvNamePokemonDetail.text = pokemon.name
            Picasso.get().load(pokemon.url).into(binding.imgPokemonDetail)

            binding.progressHP.progress = 50f
        }

        binding.imgBack.setOnClickListener {
            onBackPressed()
        }

    }
}