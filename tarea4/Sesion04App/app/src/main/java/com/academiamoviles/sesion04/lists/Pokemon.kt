package com.academiamoviles.sesion04.lists

import java.io.Serializable

data class Pokemon(
    val name:String,
    val url:String) : Serializable