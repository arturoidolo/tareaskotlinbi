package com.academiamoviles.sesion04.lists

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.academiamoviles.sesion04.databinding.ActivityPokemonBinding

class PokemonActivity : AppCompatActivity() {

    //ViewBinding
    //activity_pokemon // ActivityPokemon + Binding
    //private var binding : ActivityPokemonBinding? = null
    private lateinit var binding : ActivityPokemonBinding

    //private var adapter = PokemonAdapter()
    private lateinit var adapter : PokemonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokemonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(R.layout.activity_pokemon)

        configureAdapter()

        loadData()

        events()

    }

    private fun configureAdapter(){
        adapter = PokemonAdapter(){ pokemon ->
            val bundle = Bundle().apply {
               putSerializable("KEY_POKEMON",pokemon)
            }
            val intent = Intent(this, DetailPokemonActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }

        binding.recyclerPokedex.adapter = adapter

        /*pokemonClickedListener = object : PokemonClickedListener{
              override fun onPokemonClicked(pokemon: Pokemon) {
                 Toast.makeText(this@PokemonActivity,pokemon.name,Toast.LENGTH_SHORT).show()
              }
          }*/
    }

    private fun loadData() {


    }

    private fun events() {

        val pokemons = listOf(
            Pokemon("Bulbasaur","https://cdn.traction.one/pokedex/pokemon/1.png"),
            Pokemon("Bulbasaur2","https://cdn.traction.one/pokedex/pokemon/2.png"),
            Pokemon("Bulbasaur3","https://cdn.traction.one/pokedex/pokemon/3.png"),
            Pokemon("Bulbasaur4","https://cdn.traction.one/pokedex/pokemon/4.png"),
            Pokemon("Bulbasaur5","https://cdn.traction.one/pokedex/pokemon/5.png"),
            Pokemon("Bulbasaur6","https://cdn.traction.one/pokedex/pokemon/6.png"),
            Pokemon("Bulbasaur7","https://cdn.traction.one/pokedex/pokemon/7.png")
        )
        adapter.updateList(pokemons)
    }




}