package com.academiamoviles.sesion04.lists

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiamoviles.sesion04.R
import com.academiamoviles.sesion04.databinding.ItemPokedexBinding
import com.squareup.picasso.Picasso

//-Interfaces
//interface PokemonClickedListener{
//    fun onPokemonClicked(pokemon: Pokemon)
//}

//-Lambdas

//1. Definir la lista que manejara el adapter
//3. Implementar los metodos del adaptador
class PokemonAdapter constructor(
    var pokemons:List<Pokemon> = listOf(),
    val pokemonClickedListener: (Pokemon)->Unit)  // (Pokemon) -> Unit
    //val pokemonClickedListener: PokemonClickedListener
    : RecyclerView.Adapter<PokemonAdapter.ViewHolder>() {

    //2. Crear una clase interna denominada ViewHolder
    //-Data : pokemon
    //-Vista : item_pokedex
    inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : ItemPokedexBinding = ItemPokedexBinding.bind(itemView)

        //Scope Functions - Let / Apply / with
        fun bind(pokemon: Pokemon) = with(binding) {
            tvNamePokemon.text = pokemon.name
            Picasso.get().load(pokemon.url).error(R.drawable.images_error).into(imgPokemon)

            root.setOnClickListener {
                pokemonClickedListener(pokemon)
                //pokemonClickedListener.onPokemonClicked(pokemon)
            }

        }

    }

    fun updateList(pokemons:List<Pokemon>){
        this.pokemons = pokemons
        notifyDataSetChanged()
    }

    //Inflar una vista  //-Vista
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_pokedex,parent,false)
        return ViewHolder(view)

    }

    //Ejecutar tantas veces como elementos tenga mi lista
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pokemon = pokemons[position]
        holder.bind(pokemon)
    }

    //Cuantos elementos tiene la lista
    override fun getItemCount(): Int {
        return pokemons.size
    }

}